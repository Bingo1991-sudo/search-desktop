
"use strict"

$(document).ready(function() {
	$('.pkpoisk__cur-city').click(function(event) {
		$('.pkpoisk__advanced-search').removeClass('pkpoisk__advanced-search--active');
		$('.pkpoisk__advanced-search__text-wr').addClass('pkpoisk__advanced-search__text--hide');
		$('.pkpoisk__search-list-wr').addClass('pkpoisk__search-list-wr--active');
        $('.pkpoisk__cur-city__text').addClass('pkpoisk__cur-city__text--active');
        $('.pkpoisk__edit-btn').addClass('pkpoisk__edit-btn--active');
	});
	$('.pkpoisk__advanced-search__option').click(function(event) {
        $('.pkpoisk__advanced-search__option').removeClass('pkpoisk__advanced-search__option--active');
		$(this).toggleClass('pkpoisk__advanced-search__option--active');
	});
	$('.pkpoisk__search-list__item').click(function(event) {
		$('.pkpoisk__search-list__item').removeClass('pkpoisk__search-list__item--cur');
		$(this).toggleClass('pkpoisk__search-list__item--cur');
		$('.pkpoisk__cur-city__label').html($(this).text());
	});
    $('.pkpoisk__category-wr').click(function(event) {
        $(this).toggleClass('pkpoisk__category-wr--active');
    });
	
$(".pkpoisk__range").slider({
    min: 0,
    max: 5000,
    values: [2000, 3000],
    range: true,
    animate: "fast",
    slide : function(event, ui) {    
        $(".pkpoisk__price-filter__min").val(ui.values[ 0 ]);   
        $(".pkpoisk__price-filter__max").val(ui.values[ 1 ]);  
    }    
});
$(".pkpoisk__price-filter__min").val($(".pkpoisk__range").slider("values", 0));
$(".pkpoisk__price-filter__max").val($(".pkpoisk__range").slider("values", 1));
$(".pkpoisk__range-container input").change(function() {
    var input_left = $(".pkpoisk__price-filter__min").val().replace(/[^0-9]/g, ''),    
    opt_left = $(".pkpoisk__range").slider("option", "min"),
    where_right = $(".pkpoisk__range").slider("values", 1),
    input_right = $(".pkpoisk__price-filter__max").val().replace(/[^0-9]/g, ''),    
    opt_right = $(".pkpoisk__range").slider("option", "max"),
    where_left = $(".pkpoisk__range").slider("values", 0); 
    if (input_left > where_right) { 
        input_left = where_right; 
    }
    if (input_left < opt_left) {
        input_left = opt_left; 
    }
    if (input_left == "") {
    input_left = 0;    
    }        
    if (input_right < where_left) { 
        input_right = where_left; 
    }
    if (input_right > opt_right) {
        input_right = opt_right; 
    }
    if (input_right == "") {
    input_right = 0;    
    }    
    $(".pkpoisk__price-filter__min").val(input_left); 
    $(".pkpoisk__price-filter__max").val(input_right); 
    if (input_left != where_left) {
        $(".pkpoisk__range").slider("values", 0, input_left);
    }
    if (input_right != where_right) {
        $(".pkpoisk__range").slider("values", 1, input_right);
    }
});

});